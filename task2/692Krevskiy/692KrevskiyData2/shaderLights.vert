#version 330


//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования нормалей из локальной системы координат в систему координат камеры
uniform mat3 localToCameraMatrix;
struct LightInfo
{
    vec3 pos; //положение источника света в системе координат камеры
    vec3 La; //цвет и интенсивность окружающего света
    vec3 Ld; //цвет и интенсивность диффузного света
    vec3 Ls; //цвет и интенсивность бликового света
    float k; //коэффициент ослабления
    vec3 coneDirection; //направление света
    float coneAngle; //полуугол конуса
};
uniform LightInfo light;


out VS_OUT {
    vec3 FragPos;
    vec2 TexCoords;
    vec3 TangentLightPos;
    vec3 TangentViewPos;
    vec3 TangentFragPos;
} vs_out;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout (location = 3) in vec3 vertexTangent;
layout (location = 4) in vec3 vertexBitangent;

out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec3 normalCamSpace; //нормаль в системе координат камеры
out mat3 cameraToTangentMatrix;
out mat3 TBN;
out vec2 texCoord;
out vec3 centralPointDir;

void main()
{
    posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
    normalCamSpace = normalize(localToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры

    vs_out.TexCoords = vertexTexCoord;
    texCoord = vertexTexCoord;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    vs_out.FragPos = vec3(modelMatrix * vec4(vertexPosition, 1.0));

    vec3 T = normalize(vec3(modelMatrix * vec4(vertexTangent, 0.0)));
    vec3 N = normalize(vec3(modelMatrix * vec4(vertexNormal, 0.0)));
    // re-orthogonalize T with respect to N
    T = normalize(T - dot(T, N) * N);
    // then retrieve perpendicular vector B with the cross product of T and N
    vec3 B = cross(N, T);

    TBN = mat3(T, B, N);
    vs_out.TangentLightPos = TBN * light.pos;
    vs_out.TangentViewPos  = TBN * vec3(viewMatrix[3]);
    vs_out.TangentFragPos  = TBN * vs_out.FragPos;
    centralPointDir = (TBN * vec3((viewMatrix) * vec4(0.0, 0.0, 1, 0.0)));
 //   TBN = transpose(mat3(T, B, N));
//    vs_out.TangentFragPos  = TBN * vs_out.FragPos;
//    posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
//    normalCamSpace = normalize(localToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
//
//    texCoord = vertexTexCoord;
//
//    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);

}
